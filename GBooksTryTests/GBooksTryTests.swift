//
//  GBooksTryTests.swift
//  GBooksTryTests
//
//  Created by Consultant on 7/21/19.
//  Copyright © 2019 Consultant. All rights reserved.
//

import XCTest
@testable import GBooksTry

class GBooksTryTests: XCTestCase {

    override func setUp() {
       ViewModel.shared.session = mock
    }

    override func tearDown() {
        
    }
    
    func testService() {
        
        let promise = expectation(description: "running mock tests..")
        viewModel.getSearched(searched: "Test")
        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
            promise.fulfill()
            })
        waitForExpectations(timeout: 5, handler: nil)
        print(viewModel.searchedBooks.count)
        XCTAssert(viewModel.searchedBooks.count == 20)
        XCTAssert(viewModel.searchedBooks.count != 0)
    }

    func testBookEqual() {
        var book1: Book?
        var book2: Book?
//        var book3: Book?
        
        guard let url = URL(string: "https://www.googleapis.com/books/v1/volumes?maxResults=40&q=Harry%20Potter") else {
            
            print("API Service Failed")
            return
        }
        URLSession.shared.dataTask(with: url) { (dat, _, err) in
            
            if let error = err {
                
                print("API Request Error: \(error.localizedDescription)")
                return
            }
            
            if let data = dat {
                
                do {
                    
                    let booksJSON = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
                    if let items = booksJSON["items"] as? [[String:Any]] {
                        var books = [Book]()
                        
                        for item in items {
                            DispatchQueue.main.async{
                                let book = Book(from: item)
                                if(book.info.title != ""){
                                    books.append(book)
                                }
                                book1 = book
                                book2 = book
                            }
                            
                            
                        }
//                        book3 = books[0]
                        
                    }
                    
                    
                } catch {
                    print("Couldn't Serialiaze Object: \(error.localizedDescription)")
                }
                
            }
            
            }.resume()
        
        XCTAssert(book1 == book2)
//        XCTAssert(book1 != book3)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
