//
//  MockService.swift
//  GBooksTry
//
//  Created by Consultant on 8/8/19.
//  Copyright © 2019 Consultant. All rights reserved.
//

import Foundation

let mock = MockData()
protocol MockService {
    
    func getSearch(from search: String, completion: @escaping (([Book]) -> Void))
    
}


final class MockData: MockService {
    
    func getSearch(from search: String, completion: @escaping (([Book]) -> Void)) {
        
        let nBooks = Array(repeating: Book(), count: 20)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            completion(nBooks)
        }
    }
}
